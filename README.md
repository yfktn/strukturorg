## Plugin Struktur Organisasi / Unit Kerja

Plugin OctoberCMS ini dibuat untuk membuat struktur organisasi / unit kerja sederhana, cocok 
digunakan apabila kebutuhan anda adalah untuk membuat sebuah entri Bagian, Sub Bagian
dan unit-unit kecil lainnya dengan bentuk seperti pohon organisasi.

## Informasi Tambahan

Pada masing-masing entrian unit (selanjutnya dikatakan dengan unit untuk merujuk pada
bagian / unit kerja), bisa ditambahkan informasi tambahan yang sangat fleksibel.

Sebagai contoh: pada sebuah unit akan ditambahkan Profile, Sejarah dan isian lainnya,
maka setelah membuat isian unit baru akan terlihat isian yang bisa digunakan untuk
menambahkan informasi tersebut.

## Hak Akses

Apabila user administrator ditambahkan dengan hak akses untuk melakukan ***Entri Data Unit*** 
maka user tersebut memiliki akses untuk melakukan apa saja terhadap data yang ada.

Namun apabila user administrator ditambahkan dengan hak akses ***Update Data Unit*** 
user tersebut hanya dapat melakukan editing terhadap data unit kerja, pada unit
di mana user tersebut ditambahkan.

### Melakukan Setting Unit Untuk User Administrator

Agar bisa melakukan setting unit pada masing-masing user administrator yang telah
ditambahkan maka dibutuhkan instalasi plugin lainnya yaitu:
[Konektor Unit Pegawai User](https://gitlab.com/yfktn/pegawaistrukturuserconn).

## Komponen Informasi Tambahan

Untuk mempermudah hidup para web developer, telah dibuatkan komponen sederhana untuk
menampilkan informasi tambahan, di mana komponen ini bernama ***InformasiTambahan***.

Silahkan lakukan perubahan seperlunya berdasarkan template yang digunakan. Disarankan 
dengan menggunakan alias pada nama komponen dan menambahkan partial sebagaimana nama
alias pada theme untuk melakukan override / customisasi terhadap template,
sesuai dengan standar yang ditentukan oleh manual Octobercms.

