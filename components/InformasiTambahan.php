<?php namespace Yfktn\StrukturOrg\Components;

use Cms\Classes\ComponentBase;
use Yfktn\StrukturOrg\Models\StrukturOrg;
/**
 * Lakukan loading informasi tambahan pada halaman yang menampilkan kabar/ berita / informasi tentang
 * unit. Di sini untuk mendapatkan apakah ada informasi tambahan atau tidak bisa menggunakan 
 * variable pada halaman bernama terdapatInfoTambahan.
 * @package Yfktn\StrukturOrg\Components
 */
class InformasiTambahan extends ComponentBase
{

    public $data;
    protected $theSlug = '';

    public function componentDetails() 
    { 
        return [
            'name' => 'Informasi Tambahan',
            'description' => 'Menampilkan informasi tambahan Unit'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title' => 'Setting Slug',
                'description' => 'Unit terpilih didapatkan dari slug',
                'default' => '{{ :slug }}',
                'type' => 'string'
            ]
        ];
    }
    
    protected function prepareVars()
    {
        $this->theSlug = $this->property('slug');
    }

    protected function loadUnit()
    {
        $unit = StrukturOrg::where('slug', $this->theSlug)->first();
        // tambahkan apakah ada info tamahan?
        $this->page['terdapatInfoTambahan']= $unit->info_tambahan !== null && count($unit->info_tambahan) > 0;
        return $unit->info_tambahan;
    }

    public function onRun()
    {
        $this->prepareVars();
        $this->data = $this->loadUnit();
    }
}