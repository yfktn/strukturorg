<?php namespace Yfktn\StrukturOrg;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Yfktn\StrukturOrg\Components\InformasiTambahan' => 'informasiTambahan'
        ];
    }

    public function registerSettings()
    {
    }
}
