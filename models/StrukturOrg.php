<?php namespace Yfktn\StrukturOrg\Models;

use Model;

/**
 * Model
 */
class StrukturOrg extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\NestedTree;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'yfktn_strukturorg_utama';

    /**
     * untuk repeater field tambahan informasi history dll
     */
    protected $jsonable = ['info_tambahan'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'nama' => 'required',
        'slug'    => ['required', 'regex:/^[a-z0-9\/\:_\-\*\[\]\+\?\|]*$/i', 'unique:yfktn_strukturorg_utama'],
    ];
    
    /**
     * Dapatkan unit utama daripada unit ini, misalnya ini adalah sub bag, maka
     * kembalikan bagiannya.
     * @return Model
     */
    public function getUnitUtama():Model {
        return $this->getRoot();
    }
}
