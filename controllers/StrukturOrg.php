<?php namespace Yfktn\StrukturOrg\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;
use Exception;
use October\Rain\Support\Facades\Flash;

class StrukturOrg extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'yfktn.strukturorg.entri_struktur_organisasi',
        'yfktn.strukturorg.update_struktur_organisasi',
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Yfktn.StrukturOrg', 'struktur-organisasi');
    }

    /**
     * Lakukan filter pada query sehingga hanya menampilkan data unit yang dimiliki oleh user ini, apabila
     * hak akses yang dimiliki hanya update_struktur_organisasi!
     * @param mixed $query 
     * @return void 
     */
    public function listExtendQuery($query)
    {
        if(BackendAuth::getUser()->hasAccess('yfktn.strukturorg.update_struktur_organisasi')) {
            // check bila terdapat plugin untuk set unit kerja sudah dibuat, kita membutuhkan
            // plugin Yfktn.PegawaiStrukturUserConn
            $noUnitKerja = false;
            if(!isset(BackendAuth::getUser()->belongsToMany['unit_kerja'])) {
                // jangan ditampilkan apa-apa
                $noUnitKerja = true;
            } elseif(($unitkerja = BackendAuth::getUser()->unit_kerja()->first()) != null) {
                $parentid = $unitkerja->parent_id == null? $unitkerja->id: $unitkerja->parent_id;
                $query->where('id', $parentid);
            } elseif(!BackendAuth::getUser()->isSuperUser()) {
                // unit kerja belum diset punya orang ini
                $noUnitKerja = true;
            }
            if($noUnitKerja) {
                $query->where('id', -1);
                trace_log("StrukturOrg::listExtendQuery: hak akses update_struktur_organisasi tanpa asosiasi unit kerja!, belum menambahkan plugin Yfktn.PegawaiStrukturUserConn?");
                Flash::error("Logged User tidak punya asosiasi dengan unit kerja aktif!");
            }
        }
    }
}
