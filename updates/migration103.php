<?php namespace Yfktn\StrukturOrg\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration103 extends Migration
{
    public function up()
    {
        Schema::table('yfktn_strukturorg_utama', function($table)
        {
            $table->string('slug')->index()->nullable();
        });
    }

    public function down()
    {
        Schema::table('yfktn_strukturorg_utama', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}