<?php namespace Yfktn\StrukturOrg\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateYfktnStrukturorgUtama extends Migration
{
    public function up()
    {
        Schema::create('yfktn_strukturorg_utama', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nama', 250);
            $table->text('keterangan')->nullable();
            $table->integer('parent_id')->nullable()->unsigned();
            $table->integer('nest_left')->nullable()->unsigned();
            $table->integer('nest_right')->nullable()->unsigned();
            $table->integer('nest_depth')->nullable()->unsigned();
            $table->integer('sort_order')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('yfktn_strukturorg_utama');
    }
}