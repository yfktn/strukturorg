<?php namespace Yfktn\StrukturOrg\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration104 extends Migration
{
    public function up()
    {
        Schema::table('yfktn_strukturorg_utama', function($table)
        {
            $table->text('info_tambahan')->nullable();
        });
    }

    public function down()
    {
        Schema::table('yfktn_strukturorg_utama', function($table)
        {
            $table->dropColumn('info_tambahan');
        });
    }
}